package jcryptoboard.api.actions;

import jcryptoboard.api.model.Action;
import jcryptoboard.api.model.ActionResult;

public class EmptyAction implements Action {

    public void init() {
        // NTD
    }

    public ActionResult execute() {
        return ActionResult.OK;
    }

    public String getName() {
        return "EmptyAction";
    }
}

