package jcryptoboard.api.model;

public interface Action {

    public void init();

    public ActionResult execute() throws Exception;

    public String getName();

}
