package jcryptoboard.api.model;

public enum ActionResult {

       OK,
       NOTHING_TO_DO,       
       ERROR
       
}
