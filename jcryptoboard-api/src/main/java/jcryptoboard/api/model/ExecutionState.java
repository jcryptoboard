package jcryptoboard.api.model;

/**
 * Created with IntelliJ IDEA.
 * User: lhl
 * Date: 18.10.12
 * Time: 11:27
 * To change this template use File | Settings | File Templates.
 */
public enum ExecutionState {
    CREATED,
    RUNNING,
    FINISHED
}
