package jcryptoboard.api.model;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 26.10.12
 * Time: 13:20
 * To change this template use File | Settings | File Templates.
 */
public interface ContextAware {

    public ExecutionContext getContext();

    public void setContext(ExecutionContext ctx);

}
