package jcryptoboard.api.model;

import java.util.Map;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

public interface ExecutionContext {

    public Document getProject() ;

    public Map<String, Object> getDatabase() ;

    public String getStartActionName() ;

    public Action getStartAction();

    public ActionResult execute();

    public Map<String, Action> getWorkflow();

    public ExecutionState getState() ;

    public Logger getLogger();

    public String getWorkflowName() ;

}
