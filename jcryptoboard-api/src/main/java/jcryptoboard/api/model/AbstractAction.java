package jcryptoboard.api.model;

import jcryptoboard.api.actions.EmptyAction;
import jcryptoboard.api.model.Action;

public abstract class AbstractAction implements Action, ContextAware {

    protected Action nextAction = new EmptyAction();
    protected ExecutionContext ctx;


    public void handleInvalidInput(Exception e)
    {
        // TODO: implement
    }

    public void init() {
        // no action
    }

    public ExecutionContext getContext() {
        return ctx;
    }

    public void setContext(ExecutionContext ctx) {
        this.ctx = ctx;
    }

    public void setNextAction(Action action)
    {
        this.nextAction = action;
    }

    public String getName() {
        return getClass().getSimpleName();
    }

    @Override
    public String toString() {
        return getName();
    }
}
