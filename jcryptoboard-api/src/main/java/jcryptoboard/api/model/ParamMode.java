package jcryptoboard.api.model;

public enum ParamMode {
 In,
 Out,
 InOut
}
