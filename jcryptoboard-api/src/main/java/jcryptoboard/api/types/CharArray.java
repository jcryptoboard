package jcryptoboard.api.types;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.mutable.Mutable;

public class CharArray implements Mutable {

    private char[] array;

    public CharArray() {
        this.array  = ArrayUtils.EMPTY_CHAR_ARRAY;
    }

    public CharArray(int length) {
        this.array = new char[length];
    }

    public CharArray(String value) {
        this.array = value.toCharArray();
    }

    public void setCapacity(int capacity) {
        if (array == null) {
            array = new char[capacity];
        } else if (array.length < capacity) {
            char[] newArray = new char[capacity];
            System.arraycopy(array, 0, newArray, 0, array.length);
            array = newArray;
        }
    }

    public void setSize(int size) {
        if (array == null) {
            array = new char[size];
        } else if (array.length != size) {
            char[] newArray = new char[size];
            System.arraycopy(array, 0, newArray, 0, Math.min(array.length, size));
            array = newArray;
        }
    }

    public char[] getArray() {
        return array;
    }

    public char[] getCopy() {
        return ArrayUtils.clone(array);
    }

    public char getFirst() {
        if ((array != null) && array.length > 0)
            return array[0];

        return 0;
    }

    public char getLast() {
        if ((array != null) && array.length > 0)
            return array[array.length-1];

        return 0;
    }

    public int getLength() {
        return array.length;
    }

    public char[] getValue() {
        return array;
    }

    public String getValueAsString() {
        return new String(array);
    }

    public void setValue(Object value) {
        if (value instanceof CharSequence) {
            setValue((CharSequence) value);
        } else if (value instanceof char[]) {
            setValue((char[]) value);
        }
        throw new ClassCastException("Cannot use class: " + value.getClass().getName());
    }

    public void setValue(CharSequence value) {
        array = value.toString().toCharArray();
    }

    public void setValue(char[] value) {
        array = value;
    }

}