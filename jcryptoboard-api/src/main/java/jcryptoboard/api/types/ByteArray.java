package jcryptoboard.api.types;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.mutable.Mutable;

public class ByteArray implements Mutable {

    private byte[] array;

    public ByteArray() {
        this.array = ArrayUtils.EMPTY_BYTE_ARRAY;
    }

    public ByteArray(int length) {
        this.array = new byte[length];
    }

    public void setCapacity(int capacity) {
        if (array == null) {
            array = new byte[capacity];
        } else if (array.length < capacity) {
            byte[] newArray = new byte[capacity];
            System.arraycopy(array, 0, newArray, 0, array.length);
            array = newArray;
        }
    }

    public void setSize(int size) {
        if (array == null) {
            array = new byte[size];
        } else if (array.length != size) {
            byte[] newArray = new byte[size];
            System.arraycopy(array, 0, newArray, 0, Math.min(array.length, size));
            array = newArray;
        }
    }

    public byte[] getArray() {
        return array;
    }

    public byte[] getCopy() {
        return ArrayUtils.clone(array);
    }

    public byte getFirst() {
        if ((array != null) && array.length > 0)
            return array[0];

        return 0;
    }

    public byte getLast() {
        if ((array != null) && array.length > 0)
            return array[array.length-1];

        return 0;
    }

    public int getLength() {
        return array.length;
    }

    public byte[] getValue() {
        return array;
    }

    public void setValue(Object value) {
        array = (byte[]) value;

    }
}