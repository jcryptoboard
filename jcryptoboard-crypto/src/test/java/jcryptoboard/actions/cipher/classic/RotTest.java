package jcryptoboard.actions.cipher.classic;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 26.10.12
 * Time: 14:17
 * To change this template use File | Settings | File Templates.
 */
public class RotTest {

    protected Rot action;

    @Before
    public void setUp() throws Exception
    {
        action  = new Rot();
    }

    @Test
    public void testExecute13() throws Exception
    {
        action.getShift().setValue(13);

        action.getBuffer().setValue("tes\n tabc č° CH\nmkpioA!".toCharArray());
        action.execute();
        Assert.assertEquals("grf\n gnop č° PU\nzxcvbN!", action.getBuffer().getValueAsString());
    }


    @Test
    public void testExecute3() throws Exception
    {
        action.getShift().setValue(13);

        action.getBuffer().setValue("tes\n tabc č° CH\nmkpioA!".toCharArray());
        action.execute();
        Assert.assertEquals("whv\n wdef č° FK\npnslrD!", action.getBuffer().getValueAsString());
    }

}
