package jcryptoboard.actions.hash;

import jcryptoboard.api.actions.EmptyAction;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: lhl
 * Date: 19.10.12
 * Time: 10:18
 * To change this template use File | Settings | File Templates.
 */
public class MD5Test {

    private final byte[] HASH_EMPTY = new byte[] {
            (byte)0xd4, (byte)0x1d, (byte)0x8c, (byte)0xd9, (byte)0x8f, (byte)0x00, (byte)0xb2, (byte)0x04,
            (byte)0xe9, (byte)0x80, (byte)0x09, (byte)0x98, (byte)0xec, (byte)0xf8, (byte)0x42, (byte)0x7e };

    private final byte[] HASH_ABC = new byte[] {
            (byte)0x90, (byte)0x01, (byte)0x50, (byte)0x98, (byte)0x3c, (byte)0xd2, (byte)0x4f, (byte)0xb0,
            (byte)0xd6, (byte)0x96, (byte)0x3f, (byte)0x7d, (byte)0x28, (byte)0xe1, (byte)0x7f, (byte)0x72 };

    @Test
    public void testExecute_empty() throws Exception {
        MD5 action = new MD5();
        action.setNextAction(new EmptyAction());
        action.execute();
        Assert.assertArrayEquals(HASH_EMPTY, action.getOutput());

    }

    @Test
    public void testExecute_abc() throws Exception {
        MD5 action = new MD5();
        action.setNextAction(new EmptyAction());
        action.setData(new byte[]{'a', 'b', 'c'});
        action.execute();
        Assert.assertArrayEquals(HASH_ABC, action.getOutput());

    }

}
