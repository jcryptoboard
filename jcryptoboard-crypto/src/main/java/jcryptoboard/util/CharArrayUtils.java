package jcryptoboard.util;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 26.10.12
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */
public class CharArrayUtils {

    public static void lowercase(char[] a)
    {
        for (int i = 0; i < a.length; i++) {
            a[i] = Character.toLowerCase(a[i]);
        }
    }

    public static void uppercase(char[] a)
    {
        for (int i = 0; i < a.length; i++) {
            a[i] = Character.toUpperCase(a[i]);
        }
    }

}
