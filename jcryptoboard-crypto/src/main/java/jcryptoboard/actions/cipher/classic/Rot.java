package jcryptoboard.actions.cipher.classic;

import jcryptoboard.actions.CharFilter;
import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.model.ActionResult;
import jcryptoboard.api.types.CharArray;
import org.apache.commons.lang.mutable.MutableInt;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 26.10.12
 * Time: 12:07
 * To change this template use File | Settings | File Templates.
 */
public class Rot extends CharFilter {

    public static final int DEFAULT_SHIFT = 13;

    protected MutableInt shift = new MutableInt(DEFAULT_SHIFT);

    public MutableInt getShift() {
        return shift;
    }

    public void setShift(MutableInt shift) {
        this.shift = shift;
    }

    protected void perform()
    {
        final char[] data = buffer.getArray();
        final int s = shift.intValue();

        for (int i = 0; i < data.length; i++) {
            int c = data[i];
            if ((c >= 'a') && (c <= 'z')) {
                c = (c - 'a') + s;
                c = c % ('z' - 'a' + 1);
                c = c + 'a';
                data[i] = (char) c;
            }
            else if ((c >= 'A') && (c <= 'Z')) {
                c = (c - 'A') + s;
                c = c % ('Z' - 'A' + 1);
                c = c + 'A';
                data[i] = (char) c;
            }
        }
    }

}
