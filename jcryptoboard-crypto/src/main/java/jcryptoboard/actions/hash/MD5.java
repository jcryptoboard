package jcryptoboard.actions.hash;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.model.ActionResult;

public class MD5 extends AbstractAction {

    private MessageDigest algorithm = null;

    protected static final int MD5SUM_LENGTH = 16;

    public MD5() {

        try {
            algorithm = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Algorithm MD5 not supported",e);
        }
    }

    public ActionResult execute() throws Exception {

        algorithm.reset();
        algorithm.update(data);
        algorithm.digest(output, 0, MD5SUM_LENGTH);

        return nextAction.execute();
    }

    private byte[] data = new byte[0];
    private byte[] output = new byte[MD5SUM_LENGTH];

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        if (output == null) {
            throw new NullPointerException("Null input array passed to MD5.");
        }
        this.data = data;
    }

    public byte[] getOutput() {
        return output;
    }

    public void setOutput(byte[] output) {
        if (output == null) {
            throw new NullPointerException("Null output array passed to MD5.");
        }
        if (output.length < MD5SUM_LENGTH) {
            throw new IllegalArgumentException("Output array for MD5 too small: " + output.length);
        }

        this.output = output;
    }
}