package jcryptoboard.actions.system;

import java.io.BufferedReader;
import java.io.CharArrayWriter;
import java.io.InputStreamReader;
import java.io.Writer;
import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.model.ActionResult;

public class Exec extends AbstractAction {

    protected char[] command;

    public char[] getCommand() {
        return command;
    }

    public void setCommand(char[] command) {
        this.command = command;
    }

    public ActionResult execute() throws Exception {

        Process process = Runtime.getRuntime().exec(new String(command));

        BufferedReader b = new BufferedReader(new InputStreamReader(process.getInputStream()));

        Writer out = new CharArrayWriter();

        String line;
        while ((line = b.readLine()) != null) {
            out.append(line);
        }
        process.waitFor();
        return nextAction.execute();
    }
}
