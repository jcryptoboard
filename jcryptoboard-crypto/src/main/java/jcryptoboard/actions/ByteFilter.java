package jcryptoboard.actions;

import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.types.ByteArray;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 26.10.12
 * Time: 15:56
 * To change this template use File | Settings | File Templates.
 */
public abstract class ByteFilter extends AbstractAction {

    protected ByteArray buffer = new ByteArray();

    public ByteArray getBuffer() {
        return buffer;
    }

    public void setBuffer(ByteArray buffer) {
        this.buffer = buffer;
    }

}
