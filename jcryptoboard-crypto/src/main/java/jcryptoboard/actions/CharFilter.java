package jcryptoboard.actions;

import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.model.ActionResult;
import jcryptoboard.api.types.CharArray;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 26.10.12
 * Time: 15:56
 * To change this template use File | Settings | File Templates.
 */
public abstract class CharFilter extends AbstractAction {

    protected CharArray buffer = new CharArray();

    public CharArray getBuffer() {
        return buffer;
    }

    public void setBuffer(CharArray buffer) {
        this.buffer = buffer;
    }

    protected abstract void perform();

    @Override
    public void init() {
        perform();
    }

    public ActionResult execute() throws Exception {

        perform();
        return nextAction.execute();
    }
}
