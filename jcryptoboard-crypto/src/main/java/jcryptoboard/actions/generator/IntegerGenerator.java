package jcryptoboard.actions.generator;

import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.model.ActionResult;
import org.apache.commons.lang.mutable.MutableInt;

public class IntegerGenerator extends AbstractAction {


    public static final int DEFUALT_START = 0;
    public static final int DEFUALT_END = 100;
    public static final int DEFUALT_STEP = 1;

    protected MutableInt start = new MutableInt(DEFUALT_START);
    protected MutableInt end = new MutableInt(DEFUALT_END);
    protected MutableInt step = new MutableInt(DEFUALT_STEP);

    protected MutableInt value = new MutableInt(DEFUALT_START);

    public ActionResult execute() throws Exception {

        final int startVal = start.intValue();
        final int endVal = end.intValue();
        final int stepVal = step.intValue();

        if (stepVal > 0) {
            while (value.intValue() < endVal) {
                ActionResult ar = nextAction.execute();
                if (ar != ActionResult.OK) {
                    return ar;
                }
                value.add(stepVal);
            }
        }
        else if (stepVal < 0) {
            while (value.intValue() > endVal) {
                ActionResult ar = nextAction.execute();
                if (ar != ActionResult.OK) {
                    return ar;
                }
                value.add(stepVal);
            }
        }
        return ActionResult.OK;
    }

}
