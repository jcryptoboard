//package cz.ami.ucl;
//
//import java.util.Iterator;
//import org.apache.commons.codec.binary.Hex;
//
///**
// *
// * @author ludek
// */
//public class Generator {
//
//    private final char[] charSet;
//
//    private int length;
//
//    private final int c;
//    private final int[] state;
//    private boolean last = false;
//
//    private final byte[] result;
//
//    public Generator(char[] charSet, int length) {
//        this.charSet = charSet;
//        this.length = length;
//        this.c = charSet.length-1;
//        this.state = new int[this.length];
//        this.result = new byte[this.length];
//        for (int i = 0; i < length; i++) {
//            state[i] = 0;
//            result[i] = (byte) charSet[0];
//        }
//    }
//
//    public boolean next() {
//
//        if (last) {
//            return false;
//        }
//
//        boolean found = false;
//        for (int i = 0; i < length; i++) {
//            if (state[i] < c) {
//                state[i]++;
//                result[i] = (byte) charSet[state[i]];
//                for (int j = 0; j < i; j++) {
//                    state[j] = 0;
//                    result[j] = (byte) charSet[0];
//                }
//                found = true;
//                break;
//            }
//        }
//        if (!found) {
//            last = true;
//        }
//        return true;
//    }
//
//    public byte[] getBytes()
//    {
//        return result;
//    }
//
//    public String getResult()
//    {
//        return new String(result);
//    }
//
//}
