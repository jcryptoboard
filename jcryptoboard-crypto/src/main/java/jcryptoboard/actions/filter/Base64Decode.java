package jcryptoboard.actions.filter;

import jcryptoboard.actions.ByteToCharConverter;
import jcryptoboard.actions.CharToByteConverter;
import org.apache.commons.codec.binary.Base64;

public class Base64Decode extends CharToByteConverter {

    protected byte[] char2byte(char[] data) {
        byte[] r = new byte[data.length];
        for (int i = 0; i < data.length; i++) {
            r[i] = (byte) data[i];
        }
        return r;
    }

    @Override
    public void perform() {
        // TODO: reimplement to be faster
        byte[] base64 = char2byte(input.getArray());
        byte[] res = Base64.decodeBase64(base64);
        output.setValue(res);
    }
}
