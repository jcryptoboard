package jcryptoboard.actions.filter;

import jcryptoboard.actions.ByteToCharConverter;
import org.apache.commons.codec.binary.Hex;

public class BinToHex extends ByteToCharConverter {

    @Override
    public void perform() {
        char[] r = Hex.encodeHex(input.getArray());
        output.setValue(r);
    }
}
