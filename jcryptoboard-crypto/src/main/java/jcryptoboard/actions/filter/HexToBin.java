package jcryptoboard.actions.filter;

import jcryptoboard.actions.CharToByteConverter;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

public class HexToBin extends CharToByteConverter {

    @Override
    public void perform() {
        byte[] r = new byte[0];
        try {
            r = Hex.decodeHex(input.getArray());
        } catch (DecoderException e) {
            // TODO
            handleInvalidInput(e);
        }
        output.setValue(r);
    }
}
