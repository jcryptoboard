package jcryptoboard.actions.filter;

import jcryptoboard.actions.CharFilter;
import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.model.ActionResult;
import jcryptoboard.api.types.CharArray;
import jcryptoboard.util.CharArrayUtils;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 26.10.12
 * Time: 15:29
 * To change this template use File | Settings | File Templates.
 */
public final class LowerCase extends CharFilter {

    @Override
    protected void perform() {
        CharArrayUtils.lowercase(buffer.getArray());
    }

}
