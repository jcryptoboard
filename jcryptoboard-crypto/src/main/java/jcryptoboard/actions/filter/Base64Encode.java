package jcryptoboard.actions.filter;

import jcryptoboard.actions.ByteToCharConverter;
import org.apache.commons.codec.binary.Base64;

public class Base64Encode extends ByteToCharConverter {


    protected char[] byte2char(byte[] data) {
        char[] r = new char[data.length];
        for (int i = 0; i < data.length; i++) {
            r[i] = (char) data[i];
        }
        return r;
    }

    @Override
    public void perform() {

        byte[] res = Base64.encodeBase64(input.getArray());
        output.setValue(byte2char(res));
    }
}
