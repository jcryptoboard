package jcryptoboard.actions;

import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.model.ActionResult;
import jcryptoboard.api.types.ByteArray;
import jcryptoboard.api.types.CharArray;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 26.10.12
 * Time: 16:24
 * To change this template use File | Settings | File Templates.
 */
public abstract class ByteToCharConverter extends AbstractAction {

    protected ByteArray input = new ByteArray();
    protected CharArray output = new CharArray();

    public ByteArray getInput() {
        return input;
    }

    public void setInput(ByteArray input) {
        this.input = input;
    }

    public CharArray getOutput() {
        return output;
    }

    public void setOutput(CharArray output) {
        this.output = output;
    }

    public abstract void perform();

    @Override
    public void init() {
        perform();
    }

    public ActionResult execute() throws Exception {
        perform();
        return nextAction.execute();
    }
}
