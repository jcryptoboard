package jcryptoboard.actions.rng;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.model.ActionResult;
import org.apache.commons.lang.mutable.MutableInt;

/**
 * Created with IntelliJ IDEA.
 * User: lhl
 * Date: 18.10.12
 * Time: 17:37
 * To change this template use File | Settings | File Templates.
 */
public class Random extends AbstractAction {

    private java.util.Random rnd = new java.util.Random();

    public void setSecure(String algo) {
        try {
            rnd = SecureRandom.getInstance(algo);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException("Invalid SecureRandom algorithm: " + algo);
        }
        recompute();
    }

    private void recompute()
    {
        integer.setValue(rnd.nextInt());
        rnd.nextBytes(array);
    }

    /**
     * Updates buffers with new random values.
     * @return
     */
    public ActionResult execute() throws Exception {
        recompute();
        return nextAction.execute();
    }

    private byte[] array = new byte[8];
    private MutableInt integer = new MutableInt(0);

    public Random() {
        recompute();
    }


    /** Returns random integer */
    public void setInteger(MutableInt integer) {
        this.integer = integer;
    }

    /** Returns random integer */
    public MutableInt getInteger() {
        return integer;
    }

    /**
     * Instructs action to provide array of random bytes of given length.
     * @param length
     */
    public void setProvideArray(int length)
    {
        array = new byte[length];
    }

    public void setArray(byte[] array) {
        if (array == null) {
            throw  new NullPointerException("Array buffer for Random must not be null.");
        }
        this.array = array;
    }

    /**
     * Returns random bytes in array.
     * By default 8 bytes are provided. This can by changed by calling setArray or setProvideArray.
     */
    public byte[] getArray() {
        return array;
    }

    public String getName() {
        return "Random";
    }
}
