package jcryptoboard.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 25.10.12
 * Time: 16:51
 * To change this template use File | Settings | File Templates.
 */
public class Main {


    public static void main(String[] args) throws Exception {
        Options opts = CliOptions.buildOptions();

        CommandLineParser parser = new PosixParser();
        CommandLine cmd = parser.parse(opts, args);

        if (cmd.hasOption(CliOptions.VERBOSE)) {

        }

    }
}
