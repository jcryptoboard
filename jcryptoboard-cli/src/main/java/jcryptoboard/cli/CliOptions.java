package jcryptoboard.cli;

public class CliOptions {

    public static final String VERBOSE = "v";

    protected static org.apache.commons.cli.Options buildOptions() {
        org.apache.commons.cli.Options opts = new org.apache.commons.cli.Options();
        opts.addOption(VERBOSE, false, "verbose mode");

        return opts;
    }
}