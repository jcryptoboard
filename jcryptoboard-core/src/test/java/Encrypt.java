import java.util.Arrays;
import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.model.ActionResult;
import jcryptoboard.api.types.ByteArray;
import jcryptoboard.api.types.CharArray;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 24.10.12
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */
public class Encrypt extends AbstractAction {

    private static final Logger logger = Logger.getLogger(Encrypt.class);

    public ActionResult execute() throws Exception {
        System.out.println("encrypt executed");

        logger.info("key is " + Arrays.toString(key.getArray()));

        output.getArray()[0]++;
        return nextAction.execute();
    }

    protected CharArray key = new CharArray();
    protected ByteArray output = new ByteArray(1);
    protected CharArray cipher = new CharArray();


    public CharArray getCipher() {
        return cipher;
    }

    public void setCipher(CharArray cipher) {
        this.cipher = cipher;
    }

    public CharArray getKey() {
        return key;
    }

    public void setKey(CharArray key) {
        this.key = key;
    }

    public ByteArray getOutput() {
        return output;
    }

    public void setOutput(ByteArray output) {
        this.output = output;
    }

}
