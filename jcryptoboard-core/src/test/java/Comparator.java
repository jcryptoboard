import java.util.Arrays;
import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.model.ActionResult;
import jcryptoboard.api.types.ByteArray;
import jcryptoboard.api.types.CharArray;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 24.10.12
 * Time: 13:22
 * To change this template use File | Settings | File Templates.
 */
public class Comparator extends AbstractAction {

    private static final Logger logger = Logger.getLogger(Comparator.class);

    public ActionResult execute() throws Exception {
        System.out.println("comparator executed");
        logger.info("comparator executed");
        logger.info("value = " + Arrays.toString(value.getArray()));
        return ActionResult.OK;
    }


    public ByteArray getValue() {
        return value;
    }

    public void setValue(ByteArray value) {
        this.value = value;
    }

    protected ByteArray value = new ByteArray();
    protected CharArray expected = new CharArray();

    public CharArray getExpected() {
        return expected;
    }

    public void setExpected(CharArray expected) {
        this.expected = expected;
    }


}
