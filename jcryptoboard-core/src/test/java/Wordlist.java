import jcryptoboard.api.model.AbstractAction;
import jcryptoboard.api.model.ActionResult;
import jcryptoboard.api.types.CharArray;

/**
 * Created with IntelliJ IDEA.
 * User: lhlavace
 * Date: 24.10.12
 * Time: 13:20
 * To change this template use File | Settings | File Templates.
 */
public class Wordlist extends AbstractAction {

    public ActionResult execute() throws Exception {

        System.out.println("wordlist executed");
        String[] words = new String[] { "abc", "def", "ghj" };
        for (String w : words) {
            output.setValue(w.toCharArray());
            nextAction.execute();
        }
        return ActionResult.OK;
    }

    protected String input;
    protected CharArray output = new CharArray();


    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public CharArray getOutput() {
        return output;
    }

    public void setOutput(CharArray output) {
        this.output = output;
    }

}
