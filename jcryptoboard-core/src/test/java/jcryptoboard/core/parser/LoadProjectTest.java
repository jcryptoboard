package jcryptoboard.core.parser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Created with IntelliJ IDEA.
 * User: lhl
 * Date: 18.10.12
 * Time: 14:22
 * To change this template use File | Settings | File Templates.
 */
public class LoadProjectTest {

    protected Document project = null;

    private static final String WF_MAIN = "main";

    @Before
    public void loadProject()
    {
        InputStream is = getClass().getResourceAsStream("/sampleProject1.xml");
        project = LoadProject.loadProject(is);
    }

    @Test
    public void testLoadProject() throws Exception {
        Assert.assertNotNull(project);
    }

    @Test(expected = RuntimeException.class)
    public void testLoadProject_fail() throws Exception {
        String XML = "<JSBProject>\n<workspace>\n</JSBProject>";
        LoadProject.loadProject(new ByteArrayInputStream(XML.getBytes()));

        Assert.fail();
    }

    @Test
    public void testCheckNamesUnique() throws Exception {
        LoadProject.checkNames(project);
    }

    @Test
    public void testListWorkflows() throws Exception {

        List<String> value = LoadProject.listWorkflows(project);
        Assert.assertEquals(1, value.size());
        Assert.assertEquals(WF_MAIN, value.get(0));
    }

    @Test
    public void testGetWorkflowStart() throws Exception {
        String result = LoadProject.getWorkflowStart(project, WF_MAIN);
        Assert.assertEquals("producer", result);
    }

    @Test
    public void testGetWorkflow() throws Exception {
        Element el = LoadProject.getWorkflow(project, WF_MAIN);
        Assert.assertNotNull(el);
        NodeList list = el.getElementsByTagName(XMLConsts.ELEMENT_ACTION);
        Assert.assertEquals(3, list.getLength());
    }

    @Test(expected = RuntimeException.class)
    public void testGetWorkflow_fail() throws Exception {
        Element el = LoadProject.getWorkflow(project, "xxx");

        Assert.fail();
    }

    @Test
    public void testGetProjectDataBase() throws Exception {
        Element el = LoadProject.getProjectDatabase(project);
        Assert.assertNotNull(el);
        NodeList list = el.getElementsByTagName(XMLConsts.ELEMENT_DATA);
        Assert.assertEquals(3, list.getLength());
    }

    @Test
    public void testGetWorkflowDatabase() throws Exception {
        Element el = LoadProject.getWorkflowDatabase(project, WF_MAIN);
        Assert.assertNotNull(el);
        NodeList list = el.getElementsByTagName(XMLConsts.ELEMENT_DATA);
        Assert.assertEquals(1, list.getLength());
    }

    @Test(expected = RuntimeException.class)
    public void testGetWorkflowDatabase_fail() throws Exception {
        Element el = LoadProject.getWorkflowDatabase(project, "xxx");

        Assert.fail();
    }

}
