package jcryptoboard.core.runner;

import java.io.InputStream;
import java.util.Map;
import jcryptoboard.api.model.Action;
import jcryptoboard.api.model.ExecutionContext;
import jcryptoboard.core.parser.LoadProject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WorkflowBuilderTest {


    protected Document project = null;

    private static final String WF_MAIN = "main";

    protected WorkflowBuilder wfBuilder;

    @Before
    public void initBuilder()
    {
        wfBuilder = new WorkflowBuilder();
    }

    @Before
    public void loadProject()
    {
        InputStream is = getClass().getResourceAsStream("/sampleProject1.xml");
        project = LoadProject.loadProject(is);
    }

    @Test
    public void testLoadDatabase() throws Exception {
        Element elDb = LoadProject.getProjectDatabase(project);
        Map<String, Object> db = wfBuilder.loadDatabase(elDb);
        Assert.assertEquals(3, db.size());
        Assert.assertTrue(db.containsKey("ciphertext"));
        Assert.assertFalse(db.containsKey("tempdata1"));
    }

    @Test
    public void testLoadDatabase_wf() throws Exception {
        Element elDb = LoadProject.getWorkflowDatabase(project, WF_MAIN);
        Map<String, Object> db = wfBuilder.loadDatabase(elDb);
        Assert.assertEquals(1, db.size());
        Assert.assertTrue(db.containsKey("tempdata1"));
        Assert.assertFalse(db.containsKey("ciphertext"));
    }

    @Test
    public void testLoadWorkflow() throws Exception {
        Element elWf = LoadProject.getWorkflow(project, WF_MAIN);
        Map<String, Action> db = wfBuilder.loadWorkflow(elWf);
        Assert.assertEquals(3, db.size());
        Assert.assertTrue(db.containsKey("check"));
        Assert.assertFalse(db.containsKey("tempdata1"));
    }

    @Test
    public void testCreateWorkflow() throws Exception {
        ExecutionContext ctx = wfBuilder.createWorkflow(project, WF_MAIN);

        final Map<String, Object> database = ctx.getDatabase();
        Assert.assertEquals(4, database.size());
        Assert.assertTrue(database.containsKey("found"));
        Assert.assertTrue(database.containsKey("tempdata1"));
        Assert.assertFalse(database.containsKey("check"));
        Assert.assertFalse(database.containsKey(WF_MAIN));

        final Map<String, Action> workflow = ctx.getWorkflow();
        Assert.assertEquals(3, workflow.size());
        Assert.assertTrue(workflow.containsKey("check"));
        Assert.assertFalse(workflow.containsKey(WF_MAIN));
        Assert.assertFalse(workflow.containsKey("tempdata1"));

        Assert.assertEquals("producer", ctx.getStartActionName());
        Assert.assertEquals(WF_MAIN, ctx.getWorkflowName());

        Action a = ctx.getStartAction();
        a.execute();
    }

}
