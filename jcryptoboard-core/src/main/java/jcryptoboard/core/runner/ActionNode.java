package jcryptoboard.core.runner;

import jcryptoboard.api.model.Action;
import org.w3c.dom.Element;

/**
 * Created with IntelliJ IDEA.
 * User: lhl
 * Date: 19.10.12
 * Time: 13:42
 * To change this template use File | Settings | File Templates.
 */
class ActionNode {
    static enum State { CREATED, WIRING, COMPLETED};
    static enum ReferenceType {
        PROJECT_DB,
        WORKFLOW_DB,
        ACTION,
        ACTION_PARAM
    }

    String name;
    Element element;
    Action action;

    State state;

    ActionNode(Element element, String name, Action action) {
        this.element = element;
        this.name = name;
        this.action = action;
        this.state = State.CREATED;
    }
}
