package jcryptoboard.core.runner;

import java.util.Map;
import jcryptoboard.api.model.Action;
import jcryptoboard.api.model.ActionResult;
import jcryptoboard.api.model.ExecutionContext;
import jcryptoboard.api.model.ExecutionState;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

class ExecutionContextImpl implements ExecutionContext {

    private Document project;
    private Map<String, Object> database;
    private String workflowName;
    private String startAction;
    private Map<String, Action> workflow;
    private ExecutionState state;
    private Logger logger;

    ExecutionContextImpl() {
        this.state =  ExecutionState.CREATED;
        this.logger = Logger.getLogger(ExecutionContextImpl.class);
    }

    public Document getProject() {
        return project;
    }

    public Map<String, Object> getDatabase() {
        return database;
    }

    public String getStartActionName() {
        return startAction;
    }

    public Action getStartAction() {
        return workflow.get(startAction);
    }

    public ActionResult execute() {
        try {
            logger.info("Executing workflow");
            return getStartAction().execute();
        } catch (Exception e) {
            throw new RuntimeException("Execution terminated with exception.", e);
        }
    }

    public Map<String, Action> getWorkflow() {
        return workflow;
    }

    public ExecutionState getState() {
        return state;
    }

    public Logger getLogger() {
        return logger;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    void setProject(Document project) {
        this.project = project;
    }

    void setDatabase(Map<String, Object> database) {
        this.database = database;
    }

    void setStartAction(String startAction) {
        this.startAction = startAction;
    }

    void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    void setWorkflow(Map<String, Action> workflow) {
        this.workflow = workflow;
    }

    void setLogger(Logger logger) {
        this.logger = logger;
    }

}