package jcryptoboard.core.runner;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import jcryptoboard.api.model.Action;
import jcryptoboard.api.model.ContextAware;
import jcryptoboard.api.model.ExecutionContext;
import jcryptoboard.core.parser.LoadProject;
import jcryptoboard.core.parser.XMLConsts;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 * Created with IntelliJ IDEA.
 * User: lhl
 * Date: 17.10.12
 * Time: 16:40
 * To change this template use File | Settings | File Templates.
 */
public class WorkflowBuilder {


    public ExecutionContext createWorkflow(Document project, String workflowName)  {

        ExecutionContextImpl builder = new ExecutionContextImpl();

        builder.setProject(project);

        // data
        Element elDbP = LoadProject.getProjectDatabase(project);
        Element elDbW = LoadProject.getWorkflowDatabase(project, workflowName);
        Map<String, Object> dbP = loadDatabase(elDbP);
        Map<String, Object> dbW = loadDatabase(elDbW);
        dbP.putAll(dbW);
        builder.setDatabase(dbP);

        // workflow
        Element elWf = LoadProject.getWorkflow(project, workflowName);
        Map<String, Action> wf = loadWorkflow(elWf);
        builder.setWorkflow(wf);

        builder.setStartAction(LoadProject.getWorkflowStart(project, workflowName));

        builder.setWorkflowName(workflowName);

        wireWorkflow(builder);

        return builder;
    }


    public Map<String, Object> loadDatabase(Element database)
    {
        Map<String, Object> map = new HashMap<String, Object>();

        NodeList nodes = database.getElementsByTagName(XMLConsts.ELEMENT_DATA);

        for (int i = 0; i < nodes.getLength(); i++) {
            Element el = (Element) nodes.item(i);
            String className = el.getAttribute(XMLConsts.ATTR_CLASS);
            String name = el.getAttribute(XMLConsts.ATTR_NAME);

            try {
                Class<?> clazz = WorkflowBuilder.class.getClassLoader().loadClass(className);
                Object data = clazz.newInstance();
                map.put(name, data);

                // FIXME: implement value setup

            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("Invalid class name: "+className, e);
            } catch (InstantiationException e) {
                throw new RuntimeException("Cannot load class: "+className, e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Not allowed to load class: "+className, e);
            }
        }
        return map;
    }


    public Map<String, Action> loadWorkflow(Element workflow)
    {
        final Map<String, Action> map = new HashMap<String, Action>();

        NodeList nodes = workflow.getElementsByTagName(XMLConsts.ELEMENT_ACTION);

        for (int i = 0; i < nodes.getLength(); i++) {
            Element el = (Element) nodes.item(i);
            String className = el.getAttribute(XMLConsts.ATTR_CLASS);
            String name = el.getAttribute(XMLConsts.ATTR_NAME);

            try {
                Class<Action> clazz = (Class<Action>) WorkflowBuilder.class.getClassLoader().loadClass(className);
                if (!Action.class.isAssignableFrom(clazz)) {
                    throw new IllegalArgumentException("Class doesn't implement Action interface: "+className);
                }
                Action action = clazz.newInstance();
                map.put(name, action);

            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("Invalid class name: "+className, e);
            } catch (InstantiationException e) {
                throw new RuntimeException("Cannot load class: "+className, e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Not allowed to load class: "+className, e);
            }
        }
        return map;
    }

    /**
     *
     * @param ctx
     */
    void wireWorkflow(ExecutionContextImpl ctx) {

        final Element elWorkflow = LoadProject.getWorkflow(ctx.getProject(), ctx.getWorkflowName());

        NodeList actions = elWorkflow.getElementsByTagName(XMLConsts.ELEMENT_ACTION);

        Map<String, ActionNode> nodes = new HashMap<String, ActionNode>(2*actions.getLength());

        // create map of nodes
        for (int i = 0; i < actions.getLength(); i++) {
            Element elAction = (Element) actions.item(i);
            String name = elAction.getAttribute(XMLConsts.ATTR_NAME);
            Action action = ctx.getWorkflow().get(name);

            nodes.put(name, new ActionNode(elAction, name, action));
        }

        for (Map.Entry<String, ActionNode> e : nodes.entrySet()) {

            if (e.getValue().state == ActionNode.State.CREATED) {
                wireNode(ctx, nodes, e.getValue());
            }
        } //for
    }

    void wireNode(ExecutionContextImpl ctx, Map<String,ActionNode> nodes, ActionNode node) {

        node.state = ActionNode.State.WIRING;

        if (node.action instanceof ContextAware) {
            ContextAware a = (ContextAware) node.action;
            a.setContext(ctx);
        }

        NodeList params = node.element.getElementsByTagName(XMLConsts.ELEMENT_PARAM);

        for (int i = 0; i < params.getLength(); i++) {
            final Element elParam = (Element) params.item(i);
            final String name = elParam.getAttribute(XMLConsts.ATTR_NAME);

            // param contains reference
            if (elParam.hasAttribute(XMLConsts.ATTR_REF)) {
                final String ref = elParam.getAttribute(XMLConsts.ATTR_REF);
                final ActionNode.ReferenceType refType = getReferenceType(ctx, ref);

                // action reference
                if (ActionNode.ReferenceType.ACTION_PARAM == refType) {
                    ActionNode refAction = nodes.get(getReferenceName(ref));
                    if (refAction.state == ActionNode.State.CREATED) {
                        wireNode(ctx,  nodes, refAction);
                    }
                    if (refAction.state != ActionNode.State.COMPLETED) {
                        throw new RuntimeException("Data flow loop in workflow");
                    }

                    wireParamToAction(node, refAction, name, ref);
                }
                // action reference
                else if (ActionNode.ReferenceType.ACTION == refType) {
                    ActionNode refAction = nodes.get(getReferenceName(ref));
                    wireParamToAction(node, refAction, name, ref);
                }
                // data reference
                else {
                    wireParamToData(node, ctx, name, ref);
                }

            // param contains value
            } else {
                String value = elParam.getAttribute(XMLConsts.ATTR_VALUE);

                invokeSetter(node, name, value);
            }

        }

        node.state = ActionNode.State.COMPLETED;
    }

    private void wireParamToData(ActionNode node, ExecutionContextImpl ctx, String name, String ref) {
        String dbKey = getReferenceName(ref);
        Object dbValue = ctx.getDatabase().get(dbKey);
        invokeSetter(node, name, dbValue);
    }

    void wireParamToAction(ActionNode action, ActionNode refAction, String name, String ref) {

        final String[] refItems = StringUtils.split(ref, '.');

        if (refItems.length == 1) {
            invokeSetter(action, name, refAction.action);
        }
        else {
            String[] propItems = (String[]) ArrayUtils.remove(refItems, 0);
            String prop = StringUtils.join(propItems, '.');
            Object value = invokeGetter(refAction, prop);

            invokeSetter(action, name, value);
        }
    }

    String getReferenceName(String ref) {
        String[] refItems = StringUtils.split(ref, '.');
        if (refItems == null || refItems.length == 0) {
            throw new IllegalArgumentException("Empty reference");
        }
        return refItems[0];
    }

    ActionNode.ReferenceType getReferenceType(ExecutionContextImpl ctx, String ref) {

        String node = getReferenceName(ref);

        if (ctx.getDatabase().containsKey(node)) {
            return ActionNode.ReferenceType.WORKFLOW_DB;
        }
        if (ctx.getWorkflow().containsKey(node)) {
            if (ref.equals(node)) {
                return ActionNode.ReferenceType.ACTION;
            } else {
                return ActionNode.ReferenceType.ACTION_PARAM;
            }
        }

        throw new IllegalArgumentException("Unknown reference: " + ref);
    }

    protected PropertyUtilsBean propertyUtils = new PropertyUtilsBean();


    void invokeSetter(ActionNode node, String name, Object value) {

        Action action = node.action;
        try {
            propertyUtils.setProperty(action, name, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Cannot set param of action: " + action.getName() + "." + name,e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Setting param of action failed: " + action.getName() + "." + name,e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Param not supported by action: " + action.getName() + "." + name,e);
        }
    }

    Object invokeGetter(ActionNode node, String name) {

        Action action = node.action;
        try {
            return propertyUtils.getProperty(action, name);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Cannot set param of action: " + action.getName() + "." + name,e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Setting param of action failed: " + action.getName() + "." + name,e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Param not supported by action: " + action.getName() + "." + name,e);
        }
    }

}
