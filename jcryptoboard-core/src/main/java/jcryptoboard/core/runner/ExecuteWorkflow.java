package jcryptoboard.core.runner;


import java.io.InputStream;
import java.util.Map;
import jcryptoboard.api.model.Action;
import jcryptoboard.api.model.ActionResult;
import jcryptoboard.api.model.ExecutionContext;
import jcryptoboard.core.parser.LoadProject;
import jcryptoboard.core.parser.XMLConsts;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ExecuteWorkflow {


    public static ActionResult executeWorkflow(InputStream projectData)
    {
        return  executeWorkflow(projectData, XMLConsts.NAME_DEFAULT_WORKFLOW);
    }

    public static ActionResult executeWorkflow(InputStream projectData, String workflow_name)
    {
        WorkflowBuilder wb = new WorkflowBuilder();
        Document project = LoadProject.loadProject(projectData);
        ExecutionContext ctx = wb.createWorkflow(project, workflow_name);

        return ctx.execute();
    }

}