package jcryptoboard.core.parser;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.regex.Pattern;
import org.dom4j.DocumentException;
import org.dom4j.dom.DOMDocument;
import org.dom4j.dom.DOMDocumentFactory;
import org.dom4j.io.SAXReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LoadProject {

    public static Document loadProject(InputStream is) {


        SAXReader reader = new SAXReader(DOMDocumentFactory.getInstance(), false);

        try {
            // disble validating for now
            reader.setFeature("http://apache.org/xml/features/validation/schema", false);
            reader.setFeature("http://xml.org/sax/features/string-interning", true);
        } catch (SAXException e) {
            throw new RuntimeException("Failed to configure SAX parser",e);
        }

        Document doc = null;
        try {
            doc = (DOMDocument) reader.read(is);
        } catch (DocumentException e) {
            throw  new RuntimeException("Cannot parse XML document: ", e);
        }

        checkNames(doc);

        return doc;
    }

    protected static void checkSubElements(Set<String> ids, Element parent, String subElementName)
    {
        final Pattern namePattern = Pattern.compile(XMLConsts.PATTERN_NAME);

        NodeList nodes = parent.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if ((node.getNodeType() == Node.ELEMENT_NODE)
                    && node.getNodeName().equals(subElementName)) {
                final String name = ((Element) node).getAttribute(XMLConsts.ATTR_NAME);

                if (!namePattern.matcher(name).matches()) {
                    throw new RuntimeException("Invalid name: " + name);
                }
                if (!ids.add(name)) {
                    throw new RuntimeException("Duplicate name: " + name);
                }
            }
        }
}
    public static void checkNames(Document project) {

        final Set<String> ids = new HashSet<String>();

        final Element elPrjDb = getProjectDatabase(project);
        checkSubElements(ids, elPrjDb, XMLConsts.ELEMENT_DATA);
        final List<String> workflows = listWorkflows(project);

        for (String wfName : workflows) {
            if (!wfName.matches(XMLConsts.PATTERN_NAME)) {
                throw new RuntimeException("Invalid workflow name: " + wfName);
            }
            if (!ids.add(wfName)) {
                throw new RuntimeException("Duplicate name: " + wfName);
            }
        }
        for (String wfName : workflows) {
            final Set<String> wfIds = new HashSet<String>(ids);
            Element elWf = getWorkflow(project, wfName);
            Element elWfDb = getWorkflowDatabase(project, wfName);
            if (elWfDb != null) {
                checkSubElements(wfIds, elWfDb, XMLConsts.ELEMENT_DATA);
            }
            checkSubElements(wfIds, elWf, XMLConsts.ELEMENT_ACTION);
        }
    }

        /**
        * returns list of Workflows.
        * @param project
        * @return
        */
    public static List<String> listWorkflows(Document project) {

        List<String> result = new ArrayList<String>();

        final Element projectElement = project.getDocumentElement();

        NodeList nodes = projectElement.getElementsByTagName(XMLConsts.ELEMENT_WORKFLOW);

        for (int i = 0; i < nodes.getLength(); i++) {
            Element workflow = (Element) nodes.item(i);
            result.add(workflow.getAttribute(XMLConsts.ATTR_NAME));
        }
        return result;
    }


    public static String getWorkflowStart(Document project, String workflow_name) {

        return getWorkflow(project, workflow_name).getAttribute(XMLConsts.ATTR_START);
    }

    public static Element getWorkflow(Document project, String workflow_name) {

        List<String> result = new ArrayList<String>();

        final Element projectElement = project.getDocumentElement();
        NodeList nodesWf = projectElement.getElementsByTagName(XMLConsts.ELEMENT_WORKFLOW);

        for (int i = 0; i < nodesWf.getLength(); i++) {
            Element workflow = (Element) nodesWf.item(i);
            if (workflow.getAttribute(XMLConsts.ATTR_NAME).equals(workflow_name)) {
                return workflow;
            }
        }

        throw new NoSuchElementException("Workflow not found: " + workflow_name);
    }

    public static Map<String, Element> getWorkflowActions(Document project, String workflow_name) {

        final Map<String, Element> result = new HashMap<String, Element>();

        Element elWorkflow = getWorkflow(project, workflow_name);

        NodeList nodesWf = elWorkflow .getElementsByTagName(XMLConsts.ELEMENT_ACTION);

        for (int i = 0; i < nodesWf.getLength(); i++) {
            Element action = (Element) nodesWf.item(i);
            result.put(action.getAttribute(XMLConsts.ATTR_NAME), action);
        }

        return  result;
    }


    public static Element getProjectDatabase(Document project) {

        final Element projectElement = project.getDocumentElement();

        NodeList nodes = projectElement.getElementsByTagName(XMLConsts.ELEMENT_DATABASE);
        if (nodes.getLength() > 0) {
            return (Element) nodes.item(0);
        }

        return null;
    }

    public static Element getWorkflowDatabase(Document project, String workflowName) {


        final Element wfElement = getWorkflow(project, workflowName);

        NodeList nodes = wfElement.getElementsByTagName(XMLConsts.ELEMENT_DATABASE);
        if (nodes.getLength() > 0) {
            return (Element) nodes.item(0);
        }

        return null;
    }


}
