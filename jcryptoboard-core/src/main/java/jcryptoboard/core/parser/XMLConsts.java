package jcryptoboard.core.parser;

public  class XMLConsts {

    public static final String ELEMENT_PROJECT = "JCBProject";

    //public static final String ATTR_ID = "id";
    public static final String ATTR_NAME = "name";
    public static final String ATTR_CLASS = "class";
    public static final String ATTR_VALUE = "value";
    public static final String ATTR_REF = "ref";

    public static final String ELEMENT_DATABASE = "database";
    public static final String ELEMENT_DATA = "data";

    public static final String ELEMENT_WORKFLOW = "workflow";
    public static final String ATTR_START = "start";

    public static final String ELEMENT_ACTION = "action";
    public static final String ELEMENT_PARAM = "param";

    public static final String PATTERN_NAME ="[a-zA-Z0-9_]+";

    public static final String NAME_DEFAULT_WORKFLOW = "default";
}
